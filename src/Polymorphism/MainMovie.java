package Polymorphism;

import java.util.Scanner;

public class MainMovie {
    public static void main(String[] args) {
//        Movie newMovie = Movie.getMovie("Comedy", "Adam Sandler");
//        newMovie.watchMovie();

        Scanner s = new Scanner(System.in);
        while(true) {
            System.out.println("Vendosni llojin e filmit: ");
            String type = s.nextLine();
            System.out.println("Vendosni titullin e filmit: ");
            if ("Qq".contains(type)){
                break;
            }
            System.out.println("Enter movie title:");
            String title = s.nextLine();
            Movie movie = Movie.getMovie(type, title);
            movie.watchMovie();
        }

    }
}
