package computerfactory;

public class Product {
    private String mode;
    private String manufacturer;
    private int width;
    private int height;
    private int depth;

    public Product(String mode, String manufacturer) {
        this.mode = mode;
        this.manufacturer = manufacturer;
    }
}

class Monitor extends Product {

    private int size;
    private String resolution;
    public Monitor(String mode, String manufacturer) {
        super(mode, manufacturer);
    }

    public Monitor(String mode, String manufacturer, int size, String resolution) {
        super(mode, manufacturer);
        this.size = size;
        this.resolution = resolution;
    }

    public void drawPixels (int x, int y, String color) {
        System.out.println(String.format("Drawing pixel at %d, %d in color %s ", x, y, color));
    }
}

class Motherboard extends Product {

    private int ramSlots;
    private int cardSlots;
    private String bios;
    public Motherboard(String mode, String manufacturer) {
        super(mode, manufacturer);
    }

    public Motherboard(String mode, String manufacturer, int ramSlots, int cardSlots, String bios) {
        this(mode, manufacturer);
        this.ramSlots = ramSlots;
        this.cardSlots = cardSlots;
        this.bios = bios;
    }

    public void loadProgram (String programName) {
        System.out.println("Program " + programName + " is now loading...");
    }
}

class ComputerCase extends Product {
    private String powerSupply;
    public ComputerCase(String mode, String manufacturer) {
        super(mode, manufacturer);
    }

    public ComputerCase(String mode, String manufacturer, String powerSupply) {
        super(mode, manufacturer);
        this.powerSupply = powerSupply;
    }

    public void pressPowerButton (){
        System.out.println("Power button pressed");
    }
}