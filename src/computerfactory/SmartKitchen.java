package computerfactory;

import java.sql.Ref;
import java.sql.SQLOutput;

public class SmartKitchen {
    private Refrigerator iceBox;
    private DishWasher dishWasher;
    private CoffeMaker brewmaster;

    public SmartKitchen(Refrigerator iceBox, DishWasher dishWasher, CoffeMaker brewmaster) {
        this.iceBox = iceBox;
        this.dishWasher = dishWasher;
        this.brewmaster = brewmaster;
    }

    public void addWater (CoffeMaker brewmaster) {
        brewmaster.setHasWorkToDo(true);
    }

    public void pourMilk (Refrigerator iceBox) {
        iceBox.setHasWorkToDo(true);
    }

    public void loadDishWasher (DishWasher dishWasher) {
        dishWasher.setHasWorkToDo(true);
    }

    public void setKitchenState (CoffeMaker brewmaster, DishWasher dishWasher, Refrigerator iceBox) {
        brewmaster.setHasWorkToDo(true);
        dishWasher.setHasWorkToDo(true);
        iceBox.setHasWorkToDo(true);
    }

    public void doKitchenWork (CoffeMaker brewmaster, DishWasher dishWasher, Refrigerator iceBox) {
        brewmaster.brewCoffe();
        dishWasher.doDishes();
        iceBox.orderFood();
    }
}

class Refrigerator {
    private boolean hasWorkToDo;

    public void setHasWorkToDo(boolean hasWorkToDo) {
        this.hasWorkToDo = hasWorkToDo;
    }

    public void orderFood () {
        if (hasWorkToDo) {
            System.out.println("Food ordered");
            hasWorkToDo = false;
        }
    }
}

class DishWasher {
    private boolean hasWorkToDo;

    public void doDishes (){
        System.out.println("Dishes done");
    }

    public void setHasWorkToDo(boolean hasWorkToDo) {
        this.hasWorkToDo = hasWorkToDo;
    }
}

class CoffeMaker {
    private boolean hasWorkToDo;

    public void setHasWorkToDo(boolean hasWorkToDo) {
        this.hasWorkToDo = hasWorkToDo;
    }

    public void brewCoffe () {
        System.out.println("There is your coffe");
    }
}
