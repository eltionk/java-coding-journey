package inheritance;

public class Fish extends Animal{
    private int gills;
    private int fins;

    public Fish(String type, double weight, int gills, int fins) {
        super(type, "small", weight);
        this.gills = gills;
        this.fins = fins;
    }

    @Override
    public void move(String speed) {
        super.move(speed);
        moveMusclesFirst();
        if (speed == "fast"){
            moveBackFin();
        }
        System.out.println();
    }

    private void moveMusclesFirst () {
        System.out.println("Muscles moving");
    }

    private void moveBackFin(){
        System.out.println("Backfind moving");
    }

    @Override
    public String toString() {
        return "Fish{" +
                "gills=" + gills +
                ", fins=" + fins +
                ", type='" + type + '\'' +
                "} " + super.toString();
    }
}
