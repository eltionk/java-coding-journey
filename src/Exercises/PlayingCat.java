package Exercises;

public class PlayingCat {
    public static void main(String[] args) {
        System.out.println(isCatPlaying(true, 10));
        System.out.println(isCatPlaying(false, 36));
        System.out.println(isCatPlaying(false, 35));
        System.out.println(isCatPlaying(true, 10));
    }

    public static boolean isCatPlaying(boolean summer, int temperature){
        if (summer == false && (temperature >= 25 && temperature <= 35)) {
            return true;
        } else if (summer == true && (temperature >= 25 && temperature <= 45)) {
            return true;
        } else
            return false;
    }
    public static boolean isCatPlaying1(boolean summer, int temperature) {
            int upperLimit = summer ? 45 : 35;
            return temperature >= 25 && temperature <= upperLimit;
        }
}
