package Exercises;

import java.util.Scanner;

public class MegaBytesConverter {
    public static void main(String[] args) {
        printMegaBytesAndKiloBytes();

    }

    public static void printMegaBytesAndKiloBytes (){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please put the desired value to convert");
        int value = scanner.nextInt();

        if (value < 0) {
            System.out.println("Invalid Value");
        } else {
            int megabytes = value/1024;
            int kilobytesRemaining = value%1024;
            System.out.println(value + " KB " + "= " + megabytes + " MB " + "and " + kilobytesRemaining + " KB ");
        }
    }
}
