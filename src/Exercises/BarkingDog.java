package Exercises;

public class BarkingDog {
    public static void main(String[] args) {
        System.out.println(shouldWakeUp(true, 23));
    }

    public static boolean shouldWakeUp (boolean barking, int hourOfDay) {
        if (barking == false || hourOfDay<0 || hourOfDay>23 || (hourOfDay>7 && hourOfDay<23)) {
            return false;
        } else {
            return true;
        }
    }
}
