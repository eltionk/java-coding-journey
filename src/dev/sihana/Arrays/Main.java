package dev.sihana.Arrays;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int[] myIntArray = new int[10];
        myIntArray[5] = 50;
        myIntArray[9] = 45;
        int gjerësia = myIntArray.length;
        System.out.println(gjerësia);
        System.out.println(myIntArray[gjerësia-1]);

        double[] myDoubleArray = new double[10];
        myDoubleArray[2] = 3.5;
        System.out.println(myDoubleArray[2]);

        int[] array = new int[10];

        for (int i = 0; i<array.length; i++) {
            array[i] = i+1;
        }

        for (int i = 0; i< array.length; i++) {
            System.out.println("Vlerat e Array janë: " + array[i]);
        }

        System.out.println();

        for (int numer : array) {
            System.out.println("Vlerat në Array janë:" + numer);
        }

        System.out.println(Arrays.toString(array));
        Object objectVariable = array;
        if (objectVariable instanceof int[]) {
            System.out.println("ObjectVariable is an int array");
        }

        Object[] objectArray = new Object[3];
        objectArray[0] = "Hello";
        objectArray[1] = new StringBuilder("World");
        objectArray[2] = array;

        System.out.println(Arrays.toString(objectArray));
    }
}
