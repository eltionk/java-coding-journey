package dev.sihana.Arrays.Varargs;

import java.util.Arrays;

public class Main {
    public static void main(String... args) {
        System.out.println("Hello World");
        String[] splitStrings = "Hello World Again".split(" ");
        printText(splitStrings);
        System.out.println(Arrays.toString(splitStrings));
    }

    private static void printText (String... textList) {
        for (String t: textList) {
            System.out.println(t);
        }
    }
}
