package dev.sihana.Arrays.ArrayList;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MoreList {
    public static void main(String[] args) {
        String[] items = {"Apple", "Bananas", "Milk", "Eggs"};
        List<String> list = List.of(items);
        System.out.println(list);

        ArrayList<String> groceries = new ArrayList<>(list);
        groceries.add("Yogurt");
        System.out.println(groceries);

        ArrayList<String> nextList = new ArrayList<>(List.of("pickles", "mustard", "cheese"));
        System.out.println(nextList);

        groceries.addAll(nextList);
        System.out.println(groceries);

        System.out.println("Third item: " + groceries.get(0));

        if (groceries.contains("mustard")) {
            System.out.println("List contains mustard");
        }

        groceries.add("Yogurt");
        System.out.println(groceries);
        System.out.println("First: " + groceries.indexOf("Yogurt"));
        System.out.println("Second: " + groceries.lastIndexOf("Yogurt"));

        //Remove Certain Elements
        System.out.println(groceries);
        groceries.remove(1);
        System.out.println(groceries);
        groceries.remove("Yogurt");
        System.out.println(groceries);

//      Remove Several Elements
        groceries.retainAll(List.of("pickles", "mustard", "cheese"));
        System.out.println(groceries);

//      Remove all elements
        groceries.clear();
        System.out.println(groceries);
        System.out.println("isEmpty: " + groceries.isEmpty());

        groceries.addAll(List.of("apples", "milk", "mustard", "cheese"));
        groceries.addAll(Arrays.asList("eggs", "pickles", "mustard", "ham"));

        System.out.println(groceries);
//      Sorts item from A-Z and from 0-1
        groceries.sort(Comparator.naturalOrder());
        System.out.println(groceries);

//      Sorts item from Z-A and from 1-0
        groceries.sort(Comparator.reverseOrder());
        System.out.println(groceries);

        var groceryArray = groceries.toArray(new String[groceries.size()]);
        System.out.println(Arrays.toString(groceryArray));
    }
}
