package dev.sihana.Arrays.ArrayList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OrderedList {
    public static void main(String[] args) {
        List<String> fruits = new ArrayList<>();

        // Add elements to the list
        fruits.add("Apple");
        fruits.add("Banana");
        fruits.add("Orange");

        // Print the elements in the list
        System.out.println(fruits);

        int[] numra = new int[5];
        for (int num: numra) {
            num = 1;
        }

        System.out.println(Arrays.toString(numra));

        for (int i=0; i<numra.length; i++) {
            System.out.println(numra[i]);
        }
    }
}
