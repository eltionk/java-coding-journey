package dev.sihana.Arrays.ArrayList;

import java.util.Arrays;

public class ImmutableArray {
    public static void main(String[] args) {
        String[] originalArray = new String[] {"first", "second", "third"};
        var originalList = Arrays.asList(originalArray);

        originalList.set(0, "one");

        System.out.println(originalList);
        System.out.println(Arrays.toString(originalArray));
    }
}