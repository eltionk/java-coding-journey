package dev.sihana.Arrays.ArrayList;

import java.util.ArrayList;
import java.util.Arrays;

record GroceryItem (String name, String type, int count){
    public GroceryItem (String name) {
        this(name, "Dairy", 1);
    }
}
public class Main {
    public static void main(String[] args) {
        GroceryItem[] grocerArray = new GroceryItem[3];
        grocerArray[0] = new GroceryItem("Milk");
        grocerArray[1] = new GroceryItem("Apple", "produce", 6);
        grocerArray[2] = new GroceryItem("Oranges", "produce", 5);
        System.out.println(Arrays.toString(grocerArray));

        ArrayList objectList = new ArrayList();
        objectList.add(grocerArray);
        objectList.add("Eltion");
        System.out.println(objectList);


        ArrayList<GroceryItem> arrayLista = new ArrayList<>();
        arrayLista.add(0, new GroceryItem("eltion"));
        System.out.println(arrayLista);
    }
}
