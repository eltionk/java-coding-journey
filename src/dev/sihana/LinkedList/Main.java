package dev.sihana.LinkedList;

import java.util.LinkedList;
import java.util.ListIterator;

public class Main {
    public static void main(String[] args) {

        LinkedList<String> placesToVisit = new LinkedList<>();

        placesToVisit.add("Shkoder");
        placesToVisit.add("Budapest");
        placesToVisit.add("Istanbul");
        placesToVisit.add(0, "Mecca");

        System.out.println(placesToVisit);

        addElement(placesToVisit);
        System.out.println(placesToVisit);

//        removeElement(placesToVisit);
//        System.out.println(placesToVisit);

//        getElement(placesToVisit);
        printItinerary(placesToVisit);
        printItinerary2(placesToVisit);
        printItinerary3(placesToVisit);
    }

    public static void addElement (LinkedList<String> list) {
        list.addFirst("Madinah");
        list.addLast("Constandinople");

//      Queue Methods
        list.offer("Sarajevo");
        list.offerFirst("Pristina");
        list.offerLast("Tirana");

//      Stack Methods
        list.push("Skopje");
        list.add("Shkoder");
    }

    private static void removeElement(LinkedList<String> list) {
        list.remove(4);
        list.remove("Tirana");

//      Removes First Element
        list.remove();
        list.removeFirst();
        list.poll();
        list.pollFirst();
        list.pop();

//      Removes Last Element
        list.removeLast();
        list.pollLast();
    }

    public static void getElement (LinkedList<String> list) {
        System.out.println("Elementi në indeksin 4 të LinkedList është " + list.get(4));
        System.out.println("Elementi i parë është " + list.getFirst());
        System.out.println("Elementi i fundit është " + list.getLast());
        System.out.println("Indeksi i Istanbul është " + list.indexOf("Istanbul"));
        System.out.println("Hera e parë që shfaqet është në indeksin " + list.indexOf("Shkoder"));
        System.out.println("Hera e fundit që shfaqet është në indeksin " + list.lastIndexOf("Shkoder"));
//      Queue List
        System.out.println("Elementi i parë është " + list.element());
//      Stack List
        System.out.println("Elementi i parë është " + list.peek());
        System.out.println("Elementi i parë është " + list.peekFirst());
        System.out.println("Elementi i fundit është " + list.peekLast());
    }

    public static void printItinerary (LinkedList<String> list) {
        System.out.println("Fillon në " + list.getFirst());

        for (int i=1; i< list.size(); i++) {
            System.out.println("Nga " + list.get(i - 1) + " në " + list.get(i));
        }

        System.out.println("Mbaron në " + list.getLast());
    }

    public static void printItinerary2 (LinkedList<String> list) {
        System.out.println("Fillon në " + list.getFirst());

        String previousTown = list.getFirst();
        for (String town: list) {
            System.out.println("Nga " + previousTown + " në " + town);
            previousTown = town;

        }

        System.out.println("Mbaron në " + list.getLast());
    }

    public static void printItinerary3 (LinkedList<String> list) {
        System.out.println("Fillon në " + list.getFirst());
        ListIterator<String> iterator = list.listIterator(1);

        String previousTown = list.getFirst();
        while (iterator.hasNext()) {
            var town = iterator.next();
            System.out.println("Nga " + previousTown + " në " + town);
            previousTown = town;

        }

        System.out.println("Mbaron në " + list.getLast());
    }

    private static void testIterator(LinkedList<String> list) {

        var iterator = list.listIterator();
        while (iterator.hasNext()) {
//            System.out.println(iterator.next());
            if (iterator.next().equals("Brisbane")) {
                iterator.add("Lake Wivenhoe");
            }
        }

        while (iterator.hasPrevious()) {
            System.out.println(iterator.previous());
        }

        System.out.println(list);

        var iterator2 = list.listIterator(3);
        System.out.println(iterator2.previous());

    }
}
