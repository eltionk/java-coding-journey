package dev.sihana.LinkedList;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Scanner;

record Place (String name, int distance) {
    @Override
    public String toString() {
        return String.format("%s (%d)", name, distance);
    }
}
public class Challenge {

    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        LinkedList<Place> placesToVisit = new LinkedList<>();

        Place shkoder = new Place("Shkoder", 1374);
        addPlace(placesToVisit, shkoder);
        addPlace(placesToVisit, new Place("Tirana", 917));
        addPlace(placesToVisit, new Place("Sarandë", 3923));
        addPlace(placesToVisit, new Place("Elbasan", 2771));
        addPlace(placesToVisit, new Place("Korçë", 3972));
        addPlace(placesToVisit, new Place("Podgorica", 877));
        addPlace(placesToVisit, new Place("Pristina", 4576));


        var iterator = placesToVisit.listIterator();
        Scanner scanner = new Scanner(System.in);
        boolean quitLoop = false;
        boolean forward = true;

        printMenu();

        while (!quitLoop) {
//            if (!iterator.hasPrevious()) {
//                System.out.println("Originating : " + iterator.next());
//                forward = true;
//            }
//            if (!iterator.hasNext()) {
//                System.out.println("Final : " + iterator.previous());
//                forward = false;
//            }
            System.out.print("Enter Value: ");
            String menuItem = scanner.nextLine().toUpperCase().substring(0, 1);

            switch (menuItem) {
                case "F":
                    System.out.println("User wants to go forward");
                    if (!forward) {           // Reversing Direction
                        forward = true;
                        if (iterator.hasNext()) {
                            iterator.next();  // Adjust position forward
                        }
                    }

                    if (iterator.hasNext()) {
                        System.out.println(iterator.next());
                    }

                    break;

                case "B":
                    System.out.println("User wants to go backwards");
                    if (forward) {           // Reversing Direction
                        forward = false;
                        if (iterator.hasPrevious()) {
                            iterator.previous();  // Adjust position backwards
                        }
                    }

                    if (iterator.hasPrevious()) {
                        System.out.println(iterator.previous());
                    }
                    break;

                case "M":
                    printMenu();
                    break;

                case "L":
                    System.out.println(placesToVisit);
                    break;

                default:
                    quitLoop = true;
                    break;
            }
        }
    }



    public static void addPlace (LinkedList<Place> list, Place place) {
        if (list.contains(place)) {
            System.out.println("Found a duplicate: " + place);
            return;
        }

        for (Place p: list) {
            if (p.name().equalsIgnoreCase(place.name())){
                System.out.println("Found duplicate: " + place);
                return;
            }
        }

        int matchedIndex = 0;
        for (var listPlace: list) {
            if (place.distance() < listPlace.distance()) {
                list.add(matchedIndex, place);
                return;
            }
            matchedIndex++;
        }
        list.add(place);
    }

    private static void printMenu () {
        System.out.println("""
                Available Actions (Select word or letter):
                (F)orwards
                (B)ackwards
                (L)ist Places
                (M)enu
                (Q)uit
                """);
    }
}
