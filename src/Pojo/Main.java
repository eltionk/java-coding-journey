package Pojo;

public class Main {
    public static void main(String[] args) {
        for (int i=1; i<=5; i++){
            Student s = new Student("s92300" + i,
                    switch (i) {
                        case 1 -> "Eltion";
                        case 2 -> "Sihana";
                        case 3 -> "Eamla";
                        case 4 -> "Jonida";
                        default -> "Anonymous";
                    },
                    "03/07/1992",
                    "Java MasterClass");
            System.out.println(s);
        }
    }
}
