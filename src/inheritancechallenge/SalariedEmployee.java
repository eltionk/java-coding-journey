package inheritancechallenge;

public class SalariedEmployee extends Employee {

    public SalariedEmployee(String name, String birthDate, String endDate) {
        super(name, birthDate, endDate);
    }
}
