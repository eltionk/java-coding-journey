package inheritancechallenge;

public class Worker {
    private String name;
    private String birthDate;
    private String endDate;

    @Override
    public String toString() {
        return "Worker{" +
                "name='" + name + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }

    public Worker(String name, String birthDate, String endDate) {
        this.name = name;
        this.birthDate = birthDate;
        this.endDate = endDate;
    }

    public int getAge (int age) {
        return age;
    }

    public double collectPay (double pay) {
        return pay;
    }

    public void terminate (String endDate){

    }
}
