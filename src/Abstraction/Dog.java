package Abstraction;

public class Dog extends Mammal{
    public Dog(String type, String size, double weight) {
        super(type, size, weight);
    }

    @Override
    public void move(String speed) {
        if(speed == "Slow") {
            System.out.println(getExplicitType() + " Moving!");
        } else {
            System.out.println(getExplicitType() + " Running!");
        }
    }

    @Override
    public void shedHaid() {
        System.out.println(getExplicitType() + " sheds hair all the time!");
    }

    @Override
    public void makeNoise() {
        if (type == "Wolf") {
            System.out.println("Howling!");
        } else {
            System.out.println("Woof!");
        }
    }
}
