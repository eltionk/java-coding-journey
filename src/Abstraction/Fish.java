package Abstraction;

public class Fish extends Animal{
    public Fish(String type, String size, double weight) {
        super(type, size, weight);
    }

    @Override
    public void move(String speed) {
        if(speed == "Slow") {
            System.out.println(getExplicitType() + " Lazy swimming!");
        } else {
            System.out.println(getExplicitType() + " Darting Francically!");
        }
    }

    @Override
    public void makeNoise() {
        if (type == "Goldfish") {
            System.out.println("Swish!");
        } else {
            System.out.println("Splash!");
        }
    }
}
