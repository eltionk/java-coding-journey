package Abstraction;

public class Horse extends Mammal {
    public Horse(String type, String size, double weight) {
        super(type, size, weight);
    }

    @Override
    public void shedHaid() {
        System.out.println(getExplicitType() + "sheds in the spring");
    }

    @Override
    public void makeNoise() {

    }
}
