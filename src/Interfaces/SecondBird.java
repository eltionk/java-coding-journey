package Interfaces;

public class SecondBird extends Animal {
    public void flapWings () {
        System.out.println("Bird flapping wings");
    }

    @Override
    public void move() {
        System.out.println("Second Bird is flapping");
    }
}
