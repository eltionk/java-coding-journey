package Interfaces;

public class Main {
    public static void main(String[] args) {
        Bird bird = new Bird();
        SecondBird birdSecond = new SecondBird();
        Animal animal = birdSecond;
        FlightEnabled flier = bird;
        Trackable tracked = bird;

        animal.move();
//        flier.move();
//        tracked.move();

//        flier.takeOff();
//        flier.fly();
//        tracked.track();
//        flier.land();

//        inFlight(flier);
//        inFlight(new Jet());
//
//        Trackable truck = new Truck();
//        truck.track();
    }

    private static void inFlight (FlightEnabled flier) {
        flier.takeOff();
        flier.fly();
        if (flier instanceof Trackable tracked) {
            tracked.track();
        }
        flier.land();
    }
}
