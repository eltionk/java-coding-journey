package JavaBasics;

public class SwitchCase {
    public static void main(String[] args) {
        String month = "January";
        System.out.println(month + " is in the " + getQuarter(month) + " quarter");

        int number = 4;
        switch (number) {
            case 1:
                System.out.println("Value was 1");
                break;
            case 2:
                System.out.println("Value was 2");
                break;
            case 3: case 4: case 5:
                System.out.println("Value was neither 1 or 2");
                break;
        }

        printDayofWeek(0);
        printDayofWeek(1);
        printDayofWeek(2);
        printDayofWeek(3);
        printDayofWeek(4);
        printDayofWeek(5);
        printDayofWeek(6);
        printDayofWeek(7);
    }

    public static String getQuarter (String month) {
        return switch (month) {
            case "January", "February", "March" -> "1st";
            case "April", "May", "June" -> "2nd";
            case "July", "August", "September" -> "3rd";
            case "October", "November", "December" -> "4th";
            default -> "bad";
        };
    }

    public static void printDayofWeek (int day) {
        String dayofWeek = switch (day){
            case 0 -> {yield "Sunday";}
            case 1 -> "Monday";
            case 2 -> "Tuesday";
            case 3 -> "Wednesday";
            case 4 -> "Thursday";
            case 5 -> "Friday";
            case 6 -> "Saturday";
            default -> "Invalid Day";
        };
        System.out.println(day + " stands for " + dayofWeek);
    }
}
