package JavaBasics;

public class MethodsMain {
    public static void main(String[] args) {
        System.out.println(calculateScore(1,2,3));
        System.out.println(calculateScore(1, 2));
        System.out.println(calculateScore(2.2d, 3.4d, 5.7d));
    }

    public static int calculateScore (int x, int y, int z) {
        return x + y + z;
    }

    public static int calculateScore (int a, int b) {
        return a+b;
    }

    public static double calculateScore (double x, double y, double z) {
        return x+y+z;
    }
}
