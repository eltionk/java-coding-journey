package JavaBasics;

public class Loops {
    public static void main(String[] args) {
        System.out.println(sumOdd(1, 100));

        int[] numbers = new int[5];

        for (int i=0; i<numbers.length; i++) {
            numbers[i] = i+1;
        }

        for (int number : numbers) {
            System.out.println(number);
        }
    }

    public static boolean isOdd (int number) {
        if (number <= 0) {
            return false;
        } return number % 2 != 0;
    }

    public static int sumOdd (int start, int end) {
        if (start <= end && start < 0 && end < 0){
            return -1;
        }

        int sum = 0;
        for (int i=start; i<=end; i++) {
            if (isOdd(i)){
                 sum += i;
            }
        } return sum;

    }

    public void sumOdd2 () {

    }
}
