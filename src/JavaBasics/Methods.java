package JavaBasics;

public class Methods {
    public static void main(String[] args) {
        calculateScore(true, 10000, 8, 200);
        displayHighScorePostition("Eltion", 1);
        calculateHighScore(1000);
        System.out.println(convertToCentimeters(5));
        System.out.println(convertToCentimers(5, 10));
        checkNumber(0);

    }

    public static void calculateScore (boolean gameOver, int score, int levelCompleted, int bonus) {
        if (gameOver) {
            int finalScore = score;
            finalScore += (levelCompleted * bonus);
            System.out.println("Your final score was " + finalScore);
        }
    }

    public static void displayHighScorePostition (String playerName, int playerPosition) {
        System.out.println(playerName + " managed to get into position " + playerPosition + " on the highscore list!");
    }

    public static int calculateHighScore(int score) {
        if (score > 1000) {
            return 1;
        } else if(score >= 500 && score < 1000) {
            return 2;
        } else if (score >= 100 && score < 500) {
            return 3;
        } else {
            return 4;
        }
    }

    public static double convertToCentimeters (int height) {
        double heightCm = height*2.54d;
        return heightCm;
    }

    public static double convertToCentimers (int heightFt, int heightIn) {
        int heighFtIn = heightFt*12;
        int height = heighFtIn + heightIn;
        double heightCm = convertToCentimeters(height);
        return heightCm;
    }

    public static void checkNumber (int number) {
        if (number > 0) {
            System.out.println("Positive");
        } else if(number < 0) {
            System.out.println("Negative");
        } else {
            System.out.println("Number equal to 0");
        }
    }

    public static long toMilesPerHour (double kilometersPerHour) {
        if (kilometersPerHour<0){
            return -1;
        } else {
            return (long) (kilometersPerHour/1.6);
        }
    }
}