package JavaBasics;

public class Computer {
    private String cpu;
    private String motherboard;
    private int ram;
    private int storage;

    public Computer () {
        this(5,10);
    }

    public Computer(int ram, int storage) {
        this("AMD", "ASUS", ram, storage);
    }

    public Computer(String cpu, String motherboard, int ram, int storage) {
        this.cpu = cpu;
        this.motherboard = motherboard;
        this.ram = ram;
        this.storage = storage;
    }

    public String getCpu() {
        return cpu;
    }

    public String getMotherboard() {
        return motherboard;
    }

    public int getRam() {
        return ram;
    }

    public int getStorage() {
        return storage;
    }

    @Override
    public String toString() {
        return "JavaBasics.Computer{" +
                "cpu='" + cpu + '\'' +
                ", motherboard='" + motherboard + '\'' +
                ", ram=" + ram +
                ", storage=" + storage +
                '}';
    }
}
