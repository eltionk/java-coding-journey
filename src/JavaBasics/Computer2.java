package JavaBasics;

public class Computer2 {
    public static String name;

    public static String getName () {
        return name;
    }

    public static void setName (String myName) {
        name = myName;
    }

    public static void myStatic() {
        System.out.println("This is a static method called throught object reference");
    }
}
