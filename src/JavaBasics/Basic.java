package JavaBasics;

public class Basic {
    public static void main(String[] args) {
        int numri1 = 1; int numri2 = 2; String name = "Eltion";
        System.out.println(numri1 + numri2 + name);

        short test1 = 5;
//        short test2 = (test1/2);
        float test3 = (5.2f/2.7f);
        System.out.println(test3);

        float myFloatNumber = (float) 5.25;
        System.out.println(myFloatNumber);

        char moreUnicode = '\u0044';
        System.out.println(moreUnicode);

        char secondDec = 68;
        System.out.println(secondDec);

        String myString = "Ky është një tekst";
        System.out.println(myString);
        myString = myString + " por jo vetëm";
        System.out.println(myString);
        myString = myString.concat(" ka edhe më tepër");
        System.out.println(myString);

        char germa1 = 'A';
        char germa2 = 'B';

        System.out.println(germa1 + germa2);

        System.out.println("" + germa1 + germa2);

        int result = 2;
        if (result > 1) {
            System.out.println("Numri është i madh");
        }

        boolean isTest = false;
        if (isTest) {
            System.out.println("It is not a test");
        }
    }
}
