package JavaBasics;

public class ComputerMain {
    public static void main(String[] args) {
        Computer pc = new Computer();

        Computer pc1 = new Computer(15, 25);
        Computer pc2 = new Computer("Intel", "Asus", 50, 100);
        System.out.println(pc.toString());
        System.out.println(pc1.toString());
        System.out.println(pc2.toString());

        Computer2.setName("Sihana");
        System.out.println();
        System.out.println(Computer2.getName());

        Computer2 sihana = new Computer2();
    }
}