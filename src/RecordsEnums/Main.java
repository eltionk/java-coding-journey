package RecordsEnums;

public class Main {
    public static void main(String[] args) {
        Smartphone pixel = new Smartphone("Pixel 7", 6.3);
        System.out.println(pixel);

        DayOfWeek day = DayOfWeek.MONDAY;
        System.out.println(day);

        if (day == DayOfWeek.MONDAY) {
            System.out.println("It is almost end of week");
        }

        for (DayOfWeek Day: DayOfWeek.values()) {
            System.out.println(Day);
        }
    }
}
