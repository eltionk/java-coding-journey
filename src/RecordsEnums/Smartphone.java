package RecordsEnums;

public record Smartphone(String name, double size) {
}
