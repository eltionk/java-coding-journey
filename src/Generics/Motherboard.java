package Generics;

public class Motherboard extends Product {

    public Motherboard(String name) {
        super(name);
    }

    public void start () {
        System.out.println("Starting");
    }
}
