package Generics;

public class Printer <T extends Product> {
    T thing;

    public Printer(T thing) {
        this.thing = thing;
    }

    public void printThing () {
        System.out.println(thing.name);
    }
}
