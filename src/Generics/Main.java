package Generics;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Printer<Motherboard> motherboard = new Printer<>(new Motherboard("ASUS"));
        Printer<Cpu> cpu = new Printer<>(new Cpu("AMD"));

        shout("Eltion");
        shout(1992);
        shout (1.618);

        int [] array1 = new int[5];

        for (int i=0; i<array1.length; i++) {
            array1[i] = i;
        }

        for (int i=0; i<array1.length; i++) {
            System.out.println(array1[i]);
        }

        int [] array2 = new int[5];
        for (int i=0; i<array2.length; i++) {
            array2[i] = i;
        }

        for (int i=0; i<array2.length; i++) {
            System.out.println(array2[i]);
        }

    }

    private static <T, V> void printArray (T check, V check1) {
        if (check.equals(check1)) {
            System.out.println("They are equal");
        }
    }

    private static <T> void shout (T thingsToShout) {
        System.out.println(thingsToShout);
    }
}
